import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

const { combine, timestamp, printf, json } = winston.format;

const transports = [];

// Rotate error logs
transports.push(new DailyRotateFile({
  filename: 'logs/error-%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  level: 'error',
  maxSize: '20m',
  maxFiles: '14d'
}));

// Rotate combined logs in non-production environments or debug mode
if (process.env.NODE_ENV !== 'production' || process.env.DEBUG_MODE) {
  transports.push(new DailyRotateFile({
    filename: 'logs/combined-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    maxSize: '20m',
    maxFiles: '14d'
  }));
}

// Add console transport in non-production environments
if (process.env.NODE_ENV !== 'production') {
  const consoleFormat = printf(({ level, message, timestamp }) => {
    return `${timestamp} ${level}: ${message}`;
  });

  transports.push(new winston.transports.Console({
    format: combine(
      winston.format.colorize(),
      consoleFormat
    )
  }));
}

const logger = winston.createLogger({
  level: 'debug',
  format: combine(
    timestamp(),
    json()
  ),
  transports: transports
});

// Stream for integrating with Morgan or other middleware
logger.stream = {
  write: message => logger.info(message.trim()),
};

// Example usage
// logger.debug('Debugging information');
// logger.info('Informational message');
// logger.warn('Warning message');
// logger.error('Error message');

// try catch:
// logger.error(`Error occurred: ${error.message}`, {
//   stack: error.stack,
//   // Additional context or metadata
// });

export default logger;