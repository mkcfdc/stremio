import axios from 'axios';
import bencode from 'bencode';
import crypto from 'crypto';
import logger from './logger.js';

// Function to get InfoHash and Trackers
const getInfoHashAndTrackers = (torrentBuffer) => {
  const decodedTorrent = bencode.decode(torrentBuffer);
  const infoHash = crypto.createHash('sha1')
                         .update(bencode.encode(decodedTorrent.info))
                         .digest('hex');

                         const trackers = decodedTorrent['announce-list'] ?
                         decodedTorrent['announce-list'].flat().map((urlBuffer) => {
                             const urlString = urlBuffer.toString('utf-8'); // Make sure it's a UTF-8 string
                             //logger.debug('urlString in downloadAndProcess: 16: '+urlString); // Should log a human-readable URL
                             return `&tr=${decodeURIComponent(urlString)}`;
                         }).join('') :
                         decodedTorrent['announce'] ? `&tr=${decodeURIComponent(decodedTorrent['announce'].toString('utf-8'))}` : '';
        
        const dn = decodedTorrent['info'] && decodedTorrent['info']['name'] ?
                   `&dn=${decodeURIComponent(decodedTorrent['info']['name'].toString('utf-8'))}` : '';

  return { infoHash, trackers, dn };
};

function decodeMagnet(magnetLink) {
  const parsed = new URL(magnetLink);
  const params = new URLSearchParams(parsed.search);

  // Use Array.from to convert the entries iterator to an array
  // Then, map over the array to decode each parameter
  const decodedParams = Array.from(params.entries()).map(([key, value]) => {
    if (/^(\d+,)*\d+$/.test(value)) {
      return `${key}=${value.split(',')
        .map(num => String.fromCharCode(Number(num)))
        .join('')}`;
    }
    return `${key}=${value}`;
  });

  // Use the spread operator to convert the params array back to a string
  return `magnet:?${decodedParams.join('&')}`;
}

// Function to make a magnetLink
const makeMagnetLink = ({ infoHash, trackers, dn }) => {
  // Directly concatenate infoHash, trackers, and dn
  const mag =  `magnet:?xt=urn:btih:${infoHash}${trackers}${dn}`;
  const result = decodeMagnet(mag);
  return result;
};

export const extractInfoHashFromMagnet = (magnetLink) => {
  if (!magnetLink)
    return logger.debug("Invalid magnet link, skipping..."), null;

  const match = magnetLink.match(/btih:([a-zA-Z0-9]+)/);
  if (!match)
    return logger.debug("Failed to extract InfoHash, skipping..."), null;

  return match[1];
};

// Process the Link
export const downloadAndProcess = async (url) => {
  try {
    const { status, data, headers } = await axios.get(url, {
      responseType: 'arraybuffer',
      maxRedirects: 0,
      validateStatus: (status) => status >= 200 && status < 400,
    });

    if (status >= 300 && status < 400) {
      const magnetLink = headers['location'];
      return magnetLink ?? null;
    }

    const torrentBuffer = Buffer.from(data);
    const { infoHash, trackers, dn } = getInfoHashAndTrackers(torrentBuffer);
    const magnetLink = makeMagnetLink({ infoHash, trackers, dn });
    logger.debug("Extracted Magnetlink.");
    return magnetLink;
    
  } catch (error) {
    return null;
  }
};
