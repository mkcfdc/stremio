const qualityIcon = quality => ({ "2160p": "🌟", "1080p": "⭐", "720p": "✨", default: "❓" })[quality] || "❓";

export const formatStreamTitle = ({ Title, Seeders, Size, Tracker, quality }) =>
  `${Title} ${qualityIcon(quality)}\n👤 ${Seeders}\n💾 ${humanFileSize(Size)}\n☁️ ${Tracker}`;

export const humanFileSize = size => {
    const i = size > 0 ? Math.floor(Math.log(size) / Math.log(1024)) : 0;
    return `${(size / Math.pow(1024, i)).toFixed(2)} ${['B', 'KB', 'MB', 'GB', 'TB'][i]}`;
  };