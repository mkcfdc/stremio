#!/bin/sh

# Check if environment variables are set
if [ -n "$HCP_CLIENT_ID" ] && [ -n "$HCP_CLIENT_SECRET" ] && [ -n "$HCP_ORGANIZATION" ] && [ -n "$HCP_PROJECT" ] && [ -n "$VLT_APPLICATION_NAME" ]; then
  echo "Unsealing vault.. securely."
  vlt login || { echo "Vault login failed"; exit 1; }

  echo "Running script in the background."

  # Run vlt run in the background and capture output and errors to log files
  vlt run -c "node index.js" &
  unset HCP_CLIENT_ID HCP_CLIENT_SECRET HCP_ORGANIZATION HCP_PROJECT VLT_APPLICATION_NAME

  echo "Node.js application started and environment variables cleared."
elif [ -n "$ENCRYPTION_KEY" ] && [ -n "$JACKETT_KEY" ] && [ -n "$REDIS_URL" ]; then
  echo "Running script."
  node index.js
else
  echo "Required variables are missing."
  exit 1
fi

tail -f /dev/null