import Redis from "ioredis";
import logger from "../utils/logger.js";

export const client = new Redis(process.env.REDIS_URL);

// Log successful connection
client.on('connect', () => {
    logger.info('Redis client connected');
  });
  
  // Log connection error
  client.on('error', (err) => {
    logger.error(`Redis error: ${err.message}`);
  });
  
  // Optionally, log other events like 'reconnecting', 'end', etc.
  client.on('reconnecting', () => {
    logger.info('Redis client reconnecting');
  });

  // warn of caching options.
  if(process.env.NODE_ENV === 'dev.local') logger.debug("!!!! JACKETT CACHING IS DISABLED IN LOCAL ENV !!!!");