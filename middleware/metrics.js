// metrics.js
import client from 'prom-client';

// Create a Registry to register the metrics
const register = new client.Registry();

// Enable the collection of default metrics
client.collectDefaultMetrics({ register });

// Define custom metrics
const httpRequestCounter = new client.Counter({
  name: 'http_requests_total',
  help: 'Total number of HTTP requests',
  registers: [register],
});

const httpErrorCounter = new client.Counter({
  name: 'http_errors_total',
  help: 'Total number of HTTP error responses',
  labelNames: ['status_code'],
  registers: [register],
});

// Middleware to count requests and errors
const countRequests = (req, res, next) => {
  httpRequestCounter.inc();
  next();
};

const countErrors = (req, res, next) => {
  const originalSend = res.send;
  res.send = function (data) {
    if (res.statusCode >= 400) {
      httpErrorCounter.inc({ status_code: res.statusCode });
    }
    originalSend.call(this, data);
  };
  next();
};

// Function to expose metrics to Prometheus
const exposeMetrics = async (req, res) => {
  try {
    res.set('Content-Type', register.contentType);
    const metrics = await register.metrics(); // Await the resolved value of the Promise
    res.end(metrics);
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
    res.status(500).end(error.message);
  }
};

export { countRequests, countErrors, exposeMetrics };
