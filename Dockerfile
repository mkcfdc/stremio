# Stage 1: Build and Obfuscate the Node.js application
FROM node:20-alpine AS build-stage

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json for npm install
COPY package.json ./

# Install dependencies
RUN npm install --omit=dev
    #npm install -g javascript-obfuscator

# Copy the rest of the application code
COPY . .

# Obfuscate JavaScript files while excluding node_modules
#RUN find . -name '*.js' ! -path "./node_modules/*" -exec javascript-obfuscator {} --output {} \;

# Stage 2: Setup the final image with obfuscated code
FROM node:20-alpine

# Install Vault CLI and javascript-obfuscator
RUN apk --no-cache add gnupg curl ca-certificates openssl unzip && \
    adduser -D -u 1001 nodeuser && \
    curl -o /tmp/vlt.zip https://releases.hashicorp.com/vlt/1.0.0/vlt_1.0.0_linux_amd64.zip && \
    unzip /tmp/vlt.zip -d /tmp && \
    mv /tmp/vlt /usr/local/bin/vlt && \
    chmod +x /usr/local/bin/vlt

# Copy over the environment variables
ENV JACKETT_API="http://jackett:9117"
ENV STREMIO_URL="https://stremio.beeftaco.lol"
ENV NODE_ENV="production"
ENV PORT=7000
ENV PREMIUMIZE_API="https://www.premiumize.me/api"

#Create app directory
WORKDIR /app

#Copy the obfuscated code and other necessary files from the build stage
COPY --from=build-stage /app /app

#Copy the entrypoint script and make it executable
COPY --from=build-stage /app/entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/entrypoint.sh

# Remove entrypoint.sh from the /app directory
RUN rm /app/entrypoint.sh

#Set the entrypoint script
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]