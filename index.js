import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import compression from 'compression';
import helmet from 'helmet';

import logger from './utils/logger.js';
import morgan from 'morgan';
import { countRequests, countErrors, exposeMetrics } from './middleware/metrics.js';
import { apiLimiter } from './middleware/rateLimiter.js';

// routes
import streamRoute from './services/premiumize/routes.js';
import rdRoute from './services/rd/routes.js';
import adbRoute from './services/adb/routes.js';
import dlRoute from './services/debridlink/routes.js';
import keyValidate from './services/keyValidate/routes.js';
import stremioRoute from './services/stremio/routes.js';

const app = express();
app.set('trust proxy', 2)
app.use(bodyParser.json());
app.use(compression());
app.use(helmet());
app.use(cors());

// Apply the rate limit as middleware to all requests
app.use(apiLimiter);

// logging:
app.use((err, req, res, next) => {
  logger.error(err.message);
  res.status(500).send('Internal Server Error');
});

app.use(morgan('combined', { stream: logger.stream }));

// /stream route
app.use('/streamLink/pm', streamRoute);
app.use('/streamLink/rd', rdRoute);
app.use('/streamLink/adb', adbRoute);
app.use('/streamLink/dl', dlRoute);
app.use(keyValidate);
app.use(stremioRoute);

// metrics middleware:
app.use(countRequests);
app.use(countErrors);

process.on('unhandledRejection', (reason) => logger.error('Unhandled Promise Rejection:', reason));

// Expose metrics endpoint for Prometheus
app.get('/metrics', exposeMetrics);

const PORT = process.env.PORT || 7000;
app.listen(PORT, () => {
  logger.info(`Server running on port ${PORT}.`);
});