import axios from 'axios';

import { getDecrypted, setEncrypted } from '../../middleware/encryption.js';
import { downloadAndProcess, extractInfoHashFromMagnet } from '../../utils/downloadAndProcess.js';
import logger from '../../utils/logger.js';

const JACKETT_API_KEY = process.env.JACKETT_KEY;
const JACKETT_SERVER_URL = process.env.JACKETT_API + "/api/v2.0/indexers/test:passed/results";
const OMDB_API_KEY = process.env.OMDB_KEY; // Add this to your vault.

export async function fetchTorrentResults(searchTerm, type) {
    logger.debug('Entered fetchTorrentResults: ' + searchTerm + ' and ' + type);
    logger.debug('type: ' + type);
    logger.debug('searchTerm: ' + searchTerm);

    let showName, showYear, formattedShowName, formattedEp, formattedSeason, seriesId, season, ep;

    if (!searchTerm) {
        logger.error('Search term is required.');
        return { streams: [] };
    }

    let cacheKey;
    if (process.env.NODE_ENV !== 'dev.local') {
        cacheKey = `jackett:${searchTerm}`;
        const cachedResults = await getDecrypted(cacheKey);
        if (cachedResults) return JSON.parse(cachedResults);
    }

    if (type === 'series') {

        [seriesId, season, ep] = searchTerm.split(':');
        logger.debug('seriesId: ' + seriesId);
        logger.debug('season: ' + season);
        logger.debug('ep: ' + ep);

        let cacheName = await getDecrypted(`series:${seriesId}`);
        logger.debug('cacheName: ' + cacheName);

        if (!cacheName) {
            logger.debug('Accessing OMDBAPI.COM');

            const { data } = await axios.get('http://www.omdbapi.com', {
                params: {
                    i: seriesId,
                    apikey: OMDB_API_KEY
                },
            });

            cacheName = JSON.stringify({ showName: data.Title, year: data.Year });
            await setEncrypted(`series:${seriesId}`, cacheName, 604800);
        }

        const name = JSON.parse(cacheName);
        showName = name.showName;
        showYear = name.year?.split('\u2013')[0];
        logger.debug('showYear: ' + showYear);

        logger.debug('showName to send to jackett: ' + showName);

        const formatNumber = num => num >= 10 ? num.toString() : '0' + num;

        formattedSeason = formatNumber(season); logger.debug('season: ' + formattedSeason);
        formattedEp = formatNumber(ep); logger.debug('ep: ' + formattedEp);

        formattedShowName = `${showName} S${formattedSeason}E${formattedEp}`;
        logger.debug('formattedShowName: ' + formattedShowName);
    }

    try {
        logger.debug('Making jackett request');
        let { data } = await axios.get(JACKETT_SERVER_URL, {
            params: {
                apikey: JACKETT_API_KEY,
                t: type === 'series' ? 'tvsearch' : 'movie',
                'Category[]': type === 'series' ? 5000 : 2000,
                Query: type === 'series' ? formattedShowName : searchTerm,
            },
        });

        let results = data.Results.filter(result => result.Seeders >= 5); // Keep results with a minimum number of seeders

        if (type === 'series') {
            // Adjust the regex to account for both spaces and dots
            const formattedShowNameRegex = new RegExp(
                `^${showName.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&').replace(/ /g, '[ .]')}[ .]S${formattedSeason}E${formattedEp}`,
                'i'
            );

            results = results
                .filter(result => {
                    // Normalize title by treating dots and spaces equivalently for regex test
                    const normalizedTitle = result.Title.replace(/\./g, ' ');
                    return formattedShowNameRegex.test(normalizedTitle);
                })
                .sort((a, b) => {
                    // Normalize titles for comparison by treating dots and spaces equivalently
                    const titleA = a.Title.replace(/\./g, ' ').toUpperCase(); // Ignore case
                    const titleB = b.Title.replace(/\./g, ' ').toUpperCase(); // Ignore case
                    if (titleA < titleB) return -1;
                    if (titleA > titleB) return 1;

                    // Then, sort by seeders if titles are equivalent/similar
                    return b.Seeders - a.Seeders;
                });
        } else {
            // For movies or other types, sort only by seeders
            results.sort((a, b) => b.Seeders - a.Seeders);
        }

        results = results.slice(0, 20); // Keep only the top 20 results (this will allow for IPT results to be cached.)

        // if (results.length === 0 && type === 'series') {
        //     logger.debug('Initial Jackett request returned no results. Making a second request using newShowName');
        //     const newShowName = `${showName} S${formattedSeason}`;

        //     const { data: newData } = await axios.get(JACKETT_SERVER_URL, {
        //         params: {
        //             apikey: JACKETT_API_KEY,
        //             t: 'tvsearch',
        //             'Category[]': 5000,
        //             Query: newShowName,
        //         },
        //     });

        //     //logger.debug(JSON.stringify(newData, null, 2));

        //     results = newData.Results
        //         .filter(result => result.Seeders >= 5)
        //         .sort((a, b) => b.Seeders - a.Seeders)
        //         .slice(0, 1);
        // }


        const processResult = async (result) => {
            try {
                if (!result.InfoHash && result.Link) {
                    const magnetLink = await downloadAndProcess(result.Link);
                    result.MagnetUri = magnetLink;
                    result.InfoHash = extractInfoHashFromMagnet(magnetLink);
                }
            } catch (e) {
                console.error(`Failed to process result: ${e.message}`);
            }
        };

        // Debug results.
        //logger.debug(JSON.stringify(results, null, 2));

        // Step 1: Process only 5 IPTorrents results for download and processing
        //const iptResults = results.filter(result => result.Tracker === "IPTorrents").slice(0, 5); // Limit to first 5 IPTorrents results
        //const otherResults = results.filter(result => result.Tracker !== "IPTorrents");

        // Process IPTorrents
        //await Promise.all(iptResults.map(result => processResult(result)));

        // Process the rest:
        await Promise.all(results.map(result => processResult(result)));

        // Combined mapping, filtering, and deduplication
        const uniqueFilteredResults = Array.from(
            new Map(
                results
                    .filter(({ InfoHash, MagnetUri }) => InfoHash !== null && MagnetUri !== null)
                    .map(({ Title, Tracker, Details, Size, Seeders, Peers, InfoHash, MagnetUri }) => [InfoHash, { Title, Tracker, Details, Size, Seeders, Peers, InfoHash, MagnetUri }])
            ).values()
        );

        // Simplified caching and return logic
        if (uniqueFilteredResults.length > 0) {
            const resultsToCache = { Results: uniqueFilteredResults };
            // Assuming results.length checks the original array to decide on caching, 
            // and you might want to avoid caching if only one result is present in the original results array or a specific environmental condition is met
            if (results.length !== 1 && process.env.NODE_ENV !== 'dev.local') {
                await setEncrypted(cacheKey, JSON.stringify(resultsToCache), 604800); // Cache for a week
            }
            return resultsToCache;
        }

        return { streams: [] };
    } catch (error) {
        logger.error(`Error occurred: ${error.message}`, {
            stack: error.stack,
        });
        return { streams: [] };
    }
}