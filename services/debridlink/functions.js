import axios from 'axios';
//import crypto from 'crypto';

//import { setEncrypted, getDecrypted } from '../../middleware/encryption.js';

import logger from '../../utils/logger.js';

const STREMIO_URL = process.env.STREMIO_URL || 'https://stremio.beeftaco.lol';

function generateDirectLink(hash, apiKey) {
  const magnetLink = `magnet:?xt=urn:btih:${hash}`;
  const encodedHash = Buffer.from(`${hash}---${apiKey}`).toString('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');

  const directLink = `${STREMIO_URL}/streamLink/dl/${encodedHash}.mp4`;

  return { magnetLink, directLink };
}

async function checkTorrentAvailability(hashes, apiKey) {

  //const cacheKey = crypto.createHash('md5').update(hashes[0]).digest('hex');
  //const redisCacheKey = `checkCache:dl:${cacheKey}`;

  //const cachedData = await getDecrypted(redisCacheKey);
  //logger.debug('cachedData: ' + cachedData);
  //if (cachedData) return JSON.parse(cachedData);

  try {
    const apiUrl = 'https://debrid-link.com/api/v2/seedbox/cached?url=';

    // Join the array of hashes into a comma-delimited string
    const hashesPathSegment = hashes.join(',');
    const { data } = await axios.get(`${apiUrl}/${hashesPathSegment}`, {
      headers: { Authorization: `Bearer ${apiKey}` },
    });

    if (data.success === true) {
      const availableHashes = Object.keys(data.value);
      const results = availableHashes.map((hash) => generateDirectLink(hash, apiKey));
      //await setEncrypted(redisCacheKey, JSON.stringify(results), 86400);
      return results;

    } else {
      throw new Error('Failed to retrieve cached torrents.');
    }
  } catch (error) {
    throw error;
  }
}

export async function checkAvailability(hashes, apiKey) {
  try {
    return await checkTorrentAvailability(hashes, apiKey);
  } catch (error) {
    // Handle errors appropriately or rethrow them
    logger.error(`Error occurred: ${error.message}`);
    throw error;
  }
}

export async function getDirectLink(hash, apiKey) {

    try {
      const { data } = await axios.post('https://debrid-link.com/api/v2/seedbox/add', {
        url: hash,
        async: true
      }, {
        headers: {
          Authorization: `Bearer ${apiKey}`,
          Accept: 'application/json'
        }
      });
  
      let files = data.value.files;
      let largestFile = null;
      let largestSize = 0;
      
      for (const file of files) {
        if (file.size > largestSize) {
          largestFile = file;
          largestSize = file.size;
        }
      }
  
      if(largestFile){
        return largestFile.downloadUrl;
      }
      return null;
  
    } catch (error) {
      logger.error(error.message);
  
    }
  }