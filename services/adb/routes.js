import express from 'express';

import { getDirectLink } from './functions.js';
import { getDecrypted, setEncrypted } from '../../middleware/encryption.js';

import logger from '../../utils/logger.js';

const router = express.Router();

router.get('/:hash.mp4', async (req, res) => {
  const encodedHash = req.params.hash;

  const decodedString = Buffer.from(encodedHash.replace(/-/g, '+').replace(/_/g, '/'), 'base64').toString('utf-8');
  const [hash, apiKey] = decodedString.split('---');
  const magnetLink = 'magnet:?xt=urn:btih:' + hash;

  if (!magnetLink || !apiKey) return res.status(400).send('Missing magnetLink or apiKey');

  try {
    // Attempt to retrieve the URL from cache
    const cachedResult = await getDecrypted('adb:' + encodedHash);
    if (cachedResult){
      logger.debug('directLink grabbed from cache; '+ cachedResult);

      const cachedUrl = JSON.parse(cachedResult);
      return res.redirect(cachedUrl.directLink);
    }

    const directLinkUrl = await getDirectLink(magnetLink, apiKey);
    if (!directLinkUrl) return res.status(404).send('No direct link found');

    // Cache the URL for 24 hrs
    await setEncrypted('adb:' + encodedHash, JSON.stringify({ directLink: directLinkUrl }), 300);

    logger.debug('Redirecting to: ' + directLinkUrl);
    return res.redirect(directLinkUrl);
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
    res.status(500).send('Internal Server Error');
  }
});

export default router;