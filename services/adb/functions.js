import axios from 'axios';
import { HttpsProxyAgent } from 'https-proxy-agent';

import logger from '../../utils/logger.js';

const STREMIO_URL = process.env.STREMIO_URL || 'https://stremio.beeftaco.lol';

// Define your proxy configuration
const proxyUrl = 'http://ofbzvsey-1:83y5upw8rrsf@p.webshare.io';
// Create an instance of the HttpsProxyAgent
const agent = new HttpsProxyAgent(proxyUrl);

//checkInstantAvailability(['magnet1', 'magnet2'], apiKey)
//.then(response => logger.debug(response))
//.error(error => logger.debug(error));

function processInstantAvailabilityResponse(instantMagnets, apiKey) {
    return instantMagnets.map(({ hash }) => {
        const magnetLink = `magnet:?xt=urn:btih:${hash}`;
        const encodedHash = Buffer.from(`${hash}---${apiKey}`).toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=+$/, '');

        const directLink = `${STREMIO_URL}/streamLink/adb/${encodedHash}.mp4`;

        return { magnetLink, directLink };
    });
}

export const checkInstantAvailability = async (hashes, apiKey) => {
    const url = `https://api.alldebrid.com/v4/magnet/instant?agent=BEEFTACO&apikey=${apiKey}`;

    // Convert the array of hashes into the format required by Alldebrid
    const postData = hashes.map((magnet, index) => `magnets[]=${magnet}`).join('&');

    try {
        const response = await axios({
            method: 'post',
            url: url,
            data: postData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0'
            },
            httpAgent: agent,
        });


        //logger.debug(JSON.stringify(response.data, null, 2));

        if (response.data && response.data.data && Array.isArray(response.data.data.magnets)) {
            const instantMagnets = response.data.data.magnets.filter(({ instant }) => instant);
            return processInstantAvailabilityResponse(instantMagnets, apiKey);
        } else {
            logger.error('Unexpected response structure: ' + JSON.stringify(response, null, 2));
        }


    } catch (error) {
        logger.error(`Error occurred: ${error.message}`, {
            stack: error.stack,
            // Additional context or metadata
        });

        if (error.response) {
            const errorCode = error.response.data.error.code;
            switch (errorCode) {
                case 'MAGNET_NO_URI':
                    return { error: 'No magnet provided' };
                case 'MAGNET_INVALID_URI':
                    return { error: 'Bad magnet format' };
                default:
                    return { error: 'An error occurred while processing your request' };
            }
        }
        return { error: 'An unknown error occurred' };
    }
}

//const magnet_uri = 'your magnet uri here';
//uploadMagnet(magnet_uri)
//.then(result => logger.debug(result));
async function uploadMagnet(magnet_uri, apiKey) {
    let apiUrl = `https://api.alldebrid.com/v4/magnet/upload?agent=BEEFTACO&apikey=${apiKey}`;

    let data = 'magnets[]=' + magnet_uri;

    let config = {
        method: 'post',
        url: apiUrl,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0' // Example Mozilla user agent
        },
        data: data,
        httpAgent: agent,
    };

    try {
        let { data } = await axios(config);
        logger.debug('LINE 102 ADB Functions: ' + JSON.stringify(data, null, 2)); 

        if (data.error) {
            switch (data.error.code) {
                case 'MAGNET_NO_URI':
                    throw new Error('No magnet URI provided.');
                case 'MAGNET_INVALID_URI':
                    throw new Error('The provided magnet URI is not valid.');
                case 'MAGNET_MUST_BE_PREMIUM':
                    throw new Error('This feature is only available for premium users.');
                case 'NO_SERVER':
                    throw new Error('Servers are not allowed to use this feature.');
                case 'MAGNET_TOO_MANY_ACTIVE':
                    throw new Error('You have reached the maximum number of active magnets.');
                // ... handle other error codes
                default:
                    throw new Error('An unknown error occurred.');
            }
        }

        let magnets = data.data.magnets;
        for (let magnet of magnets) {
            if (magnet.ready) {
                return {
                    hash: magnet.hash,
                    id: magnet.id
                };
            } else {
                logger.debug('No magnets ready.');
            }
        }
        return null; // Returns null if no magnet is ready
    } catch (error) {
        logger.error(`LINE 135. adb functions: Error occurred: ${error}`, {
            stack: error.stack,
            // Additional context or metadata
        });
        return null; // Returns null in case of an error
    }
}

//getLink(magnetId, apiKey)
//    .then(link => logger.debug('Link:', link))
//    .catch.error(e => logger.debug(e));
async function getLink(magnetId, apiKey) {
    try {
        const { data } = await axios.get(
            `https://api.alldebrid.com/v4/magnet/status?agent=BEEFTACO&apikey=${apiKey}&status=ready&id=${magnetId}`, {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0',
            httpAgent: agent,
        });

        if (!data?.data?.magnets?.links?.length) {
            logger.error('No magnets or links received.');
        }

        // Assume largest link is the first link initially
        let largestLink = data.data.magnets.links[0];
        let largestSize = largestLink.size;

        // Loop through each link and check its size
        for (let link of data.data.magnets.links) {
            if (link.size > largestSize) {
                largestSize = link.size;
                largestLink = link;
            }
        }

        // Return the largest link
        return largestLink.link;

    } catch (err) {
        logger.error(`Error occurred: ${err.message}`, {
            stack: err.stack,
            // Additional context or metadata
        });
        return null;
    }
}

//unlockLink(link, apiKey)
//.then(response => logger.debug(response))
//.catch(error => logger.debug(error));
async function unlockLink(link, apiKey) {
    const url = `https://api.alldebrid.com/v4/link/unlock?agent=BEEFTACO&apikey=${apiKey}&link=${link}`;

    try {
        const { data } = await axios.get(url, {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0',
        httpAgent: agent 
        });

        const link = data.data.link;

        return link; // The returned data from the API call
    } catch (err) {
        logger.error(`LINE 186 adb functions: Error occurred: ${err.message}`, {
            stack: err.stack,
            // Additional context or metadata
        });
    }
}

//const directLink = await getDirectLink(magnet, apiKey);
//logger.debug(directLink);
export async function getDirectLink(magnetLink, apiKey) {
    const upload = await uploadMagnet(magnetLink, apiKey);
    if (!upload.id) return logger.debug('No uploadMagnet ID. ADB functions.js 197');
    const link = await getLink(upload.id, apiKey);
    if (!link) return logger.debug('No link returned. adb functions 199.');
    const directLink = await unlockLink(link, apiKey);
    return directLink;
}