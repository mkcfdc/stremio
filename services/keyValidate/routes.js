import express from 'express';
import logger from '../../utils/logger.js';

import { validateApiKey } from './functions.js';

const router = express.Router();

// Define the route for validating an API key
router.post('/validateApiKey', async (req, res) => {
    try {
        // Extract the apiKey from the request body
        const { apiKey, service, uid } = req.body;

        if(!service || !uid){
            return  res.status(400).json({ message: "Missing parameters" });
        }

        // Perform the API key validation logic
        const encryptedKey = await validateApiKey(apiKey, service, uid);
        if (!encryptedKey) return res.status(401).json({ status: 401, message: 'Invalid API key' });

        logger.debug('We got the key. Returning 200: ' + encryptedKey);
        res.status(200).json({
            status: 200,
            encryptedKey: encryptedKey,
        });

    } catch (error) {
        logger.error('Error validating API key:', error);
        res.status(500).json({
            status: 'error',
            message: 'Internal server error',
        });
    }
});

export default router;