import axios from 'axios';
import crypto from 'crypto';
import logger from '../../utils/logger.js';

const { API_ENCRYPTION_KEY: SECRET_KEY } = process.env;

// Helper function to make an API call and return true if the key/token is valid, false otherwise
async function callApi(apiUrl, keyOrToken, isBearerToken = false, successCondition = (data) => data && data.status === 'success') {
    try {
        let headers = {};

        if (isBearerToken) {
            headers['Authorization'] = `Bearer ${keyOrToken}`;
        } else {
            apiUrl += `?apikey=${encodeURIComponent(keyOrToken)}`;
        }

        const { data } = await axios.get(apiUrl, { headers });

        return successCondition(data);
    } catch (error) {
        logger.error(`Error calling API: ${apiUrl}`, error);
        return false;
    }
}

async function callPremiumize(key) {
    const apiUrl = `https://premiumize.me/api/account/info`;
    return await callApi(apiUrl, key, true); // Using Bearer token
}

async function callAllDebrid(key) {
    const apiUrl = `https://api.alldebrid.com/v4/user?agent=BEEFTACO`;
    return await callApi(apiUrl, key);
}

async function callRealDebrid(apiToken) {
    const apiUrl = 'https://api.real-debrid.com/rest/1.0/user';
    return await callApi(apiUrl, apiToken, true, data => data && data.premium > 0);
}

// async function callDebridLink(apiToken) {
//     const apiUrl = 'https://debrid-link.com/api/v2/account/infos';
//     return callApi(apiUrl, apiToken, true, data => data && data.success === true);
// }

async function callDebridLink(apiToken) {
    const apiUrl = 'https://debrid-link.com/api/v2/account/infos';
    return await callApi(apiUrl, apiToken, true, data => {
        if (data && data.success === true) {
            return data;
        } else {
            return null;
        }
    });
}

const serviceValidationFunctions = {
    'pm': callPremiumize,
    'adb': callAllDebrid,
    'rd': callRealDebrid,
    'dl': callDebridLink,
};

async function encryptKey(apiKey) {
    const iv = crypto.randomBytes(16);

    const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(SECRET_KEY, 'hex'), iv);

    let encryptedData = cipher.update(apiKey, 'utf8', 'base64');
    encryptedData += cipher.final('base64');

    const combinedData = Buffer.concat([iv, Buffer.from(encryptedData, 'base64')]);
    const urlFriendlyEncryptedData = encodeURIComponent(combinedData.toString('base64'));

    return urlFriendlyEncryptedData;
}

export async function validateApiKey(apiKey = null, service = 'user', uid) {

    // Handle special user case without debrid service
    if (service === 'user') {
        const apiString = 'user-' + uid;
        return encryptKey(apiString);
    }

    // Check if the service is valid and call the respective API validation function
    const validateFunction = serviceValidationFunctions[service];
    if (!validateFunction) {
        console.error('No validation function for service:', service);
        return false;
    }

    try {
        const isValid = await validateFunction(apiKey);
        if (isValid) {
            console.log('API key is valid for service:', service);
            const apiString = service + '-' + apiKey + '||' + uid;
            return encryptKey(apiString);
        } else {
            console.error('API key is invalid for service:', service);
        }
    } catch (error) {
        console.error('Error validating API key for service:', service, error);
    }
    return false;
}