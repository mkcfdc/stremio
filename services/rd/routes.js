import express from "express";

// Import your modules
import * as rd from "./functions.js";
import { getDecrypted, setEncrypted } from '../../middleware/encryption.js';
import logger from "../../utils/logger.js";

const router = express.Router();

router.get('/:hash.mp4', async (req, res) => {
  const encodedHash = req.params.hash;

  const decodedString = Buffer.from(encodedHash.replace(/-/g, '+').replace(/_/g, '/'), 'base64').toString('utf-8');
  const [hash, apiKey] = decodedString.split('---');
  const magnetLink = 'magnet:?xt=urn:btih:' + hash;

  if (!magnetLink || !apiKey) return res.status(400).send('Missing magnetLink or apiKey');

  try {
    // Attempt to retrieve the URL from cache
    const cachedUrl = await getDecrypted('rd:' + encodedHash);
    const json = JSON.parse(cachedUrl);
    if (cachedUrl) return res.redirect(json.directLink);

    const directLinkUrl = await rd.processTorrent(magnetLink, apiKey);
    if (!directLinkUrl) return res.status(404).send('No direct link found');

    // Cache the URL for 5 minutes (300,000 milliseconds)
    await setEncrypted('rd:' + encodedHash, JSON.stringify({ directLink: directLinkUrl }), 300);

    logger.debug('Redirecting to: ' + directLinkUrl);
    return res.redirect(directLinkUrl);
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
    res.status(500).send('Internal Server Error');
  }
});

export default router;