import axios from 'axios';
//import crypto from 'crypto';

//import { getDecrypted, setEncrypted } from '../../middleware/encryption.js';
import logger from '../../utils/logger.js';

const STREMIO_URL = process.env.STREMIO_URL || 'https://stremio.beeftaco.lol';
const API_BASE_URL = 'https://api.real-debrid.com/rest/1.0';

function processInstantAvailabilityResponse(response, apiKey) {
  const streamLinks = Object.keys(response).map(hash => {
    // Encode each hash to create part of the direct link
    const magnetLink = `magnet:?xt=urn:btih:${hash}`;
    const encodedHash = Buffer.from(`${hash}---${apiKey}`).toString('base64')
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
      .replace(/=+$/, '');

    const directLink = `${STREMIO_URL}/streamLink/rd/${encodedHash}.mp4`;

    return { magnetLink, directLink: directLink };
  });
  return streamLinks;
}

export async function checkAvailability(hashes, apiKey) {
  //const cacheKey = crypto.createHash('md5').update(hashes[0]).digest('hex');
  //const redisCacheKey = `checkCache:rd:${cacheKey}`;

  //const cachedData = await getDecrypted(redisCacheKey);
  //logger.debug('cachedData: ' + cachedData);
  //if (cachedData) return processInstantAvailabilityResponse(JSON.parse(cachedData));


  try {
    // Join the array of hashes into a string separated by slashes.
    const hashesPathSegment = hashes.join('/');

    const { data } = await axios.get(`${API_BASE_URL}/torrents/instantAvailability/${hashesPathSegment}`, {
      headers: { 'Authorization': `Bearer ${apiKey}` }
    });


    // Filter out any hash where 'rd' is empty.
    const filteredData = Object.entries(data).reduce((acc, [key, value]) => {
      if (value.rd && value.rd.length > 0) {
        acc[key] = value; // Keep the item if 'rd' is not empty
      }
      return acc;
    }, {});

    //logger.debug(JSON.stringify(filteredData, null, 2));

    //await setEncrypted(redisCacheKey, JSON.stringify(data), 86400);

    return processInstantAvailabilityResponse(filteredData, apiKey);
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}

async function addMagnet(magnetLink, apiKey) {
  try {
    const response = await axios.post(`${API_BASE_URL}/torrents/addMagnet`, `magnet=${encodeURIComponent(magnetLink)}`, {
      headers: { 'Authorization': `Bearer ${apiKey}` }
    });
    return response.data;
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}

async function getTorrentInfo(id, apiKey) {
  try {
    const response = await axios.get(`${API_BASE_URL}/torrents/info/${id}`, {
      headers: { 'Authorization': `Bearer ${apiKey}` }
    });
    return response.data;
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}

async function selectFiles(torrentId, fileIds, apiKey) {
  try {
    const response = await axios.post(`${API_BASE_URL}/torrents/selectFiles/${torrentId}`, `files=${fileIds}`, {
      headers: { 'Authorization': `Bearer ${apiKey}` }
    });
    return response.data;
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}

async function unrestrictLink(link, apiKey) {
  try {
    const response = await axios.post(`${API_BASE_URL}/unrestrict/link`, `link=${encodeURIComponent(link)}`, {
      headers: { 'Authorization': `Bearer ${apiKey}` }
    });
    return response.data;
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}

export async function processTorrent(magnetLink, apiKey) {
  try {
    // Extract hash from magnet link for availability check
    const hashMatch = magnetLink.match(/btih:([a-zA-Z0-9]*)/i);
    if (!hashMatch) {
      logger.error('Invalid magnet link');
      return;
    }

    // Add magnet
    const addResult = await addMagnet(magnetLink, apiKey);

    // Get torrent info to find files
    const torrentId = addResult.uri.split('/').pop(); // Assuming URI contains torrent ID as the last segment
    const torrentInfo = await getTorrentInfo(torrentId, apiKey);

    // Find the largest file by bytes
    let largestFileId = null;
    let maxBytes = 0;
    torrentInfo.files.forEach(file => {
      if (file.bytes > maxBytes) {
        maxBytes = file.bytes;
        largestFileId = file.id.toString(); // Ensure file.id is a string, as expected by the API
      }
    });

    if (largestFileId === null) {
      logger.error('No file selected');
      return;
    }

    // Select the largest file
    await selectFiles(torrentId, largestFileId, apiKey);

    // Get updated torrent info, which should now include links
    const updatedTorrentInfo = await getTorrentInfo(torrentId, apiKey);
    if (updatedTorrentInfo.links && updatedTorrentInfo.links.length > 0) {
      // Unrestrict the first link (assuming it's the one you want)
      const unrestrictedLinkResult = await unrestrictLink(updatedTorrentInfo.links[0], apiKey);
      //logger.debug('Unrestricted Link:', unrestrictedLinkResult.download);
      return unrestrictedLinkResult.download;
    } else {
      logger.debug('No unrestricted links found.');
    }
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}