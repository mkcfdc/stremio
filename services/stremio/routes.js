import express from 'express';

import crypto from 'crypto';

import { handleStreams, validateIMDbID } from './functions.js'

import logger from '../../utils/logger.js';
import { client } from '../../middleware/redisClient.js';

const router = express.Router();

const VERSION = '2.2.0';
const DESCRIPTION = 'BeefTaco uses 32 different indexes. Debrid supported. Now with Series support.';

let manifestName = "BeefTaco Magnet Search";
if (process.env.NODE_ENV === "dev.local") {
  manifestName += " LOCAL";
}

const STREMIO_URL = process.env.STREMIO_URL || 'https://stremio.beeftaco.lol';


router.get('/clearCache/:imdbId', validateIMDbID, (req, res) => {
  const imdbId = req.params.imdbId;
  const cacheName = 'jackett:' + imdbId;

  client.exists(cacheName, (err, reply) => {
    if (err) return res.status(500).send('Internal Server Error');
    if (reply !== 1) return res.status(404).send(`Cache "${cacheName}" not found`);

    client.ttl(cacheName, (err, ttl) => {
      if (err) return res.status(500).send('Internal Server Error');
      if (ttl < 0) return res.status(400).send('Cache has no or expired TTL');

      if (ttl <= (604800 - 86400)) {
        client.del(cacheName, (err) => {
          if (err) return res.status(500).send('Internal Server Error');
          logger.debug(`Cache ${cacheName} deleted successfully.`);
          //res.redirect('//detail/movie/' + req.params.cacheName);
          res.status(200).json({ status: 200, message: `${imdbId} successfully deleted from cache. Please search again.` });
        });
      } else {
        res.send(`Cache ${cacheName} is not yet 24 hours old.`);
      }
    });
  });
});


router.get('/configure', (req, res) => {
  res.redirect('https://beeftaco.lol/dashboard');
});

router.get('/:userToken/manifest.json', (req, res) => {
  // Serve a dynamic manifest based on the user token
  const userToken = req.params.userToken;

  const manifest = {
    id: "org.beef.taco." + userToken,
    version: VERSION,
    name: manifestName,
    description: DESCRIPTION,
    userData: userToken,
    resources: [
      {
        name: "stream",
        types: ["movie", "series"],
      }
    ],
    types: ["movie", "series"],
    catalogs: [],
    idPrefixes: ["tt"]
  };

  logger.debug('Config loaded with API KEY: ' + userToken);
  if (userToken === "V1dO4N9p2%2BgUQ5a4cpQ35E5iRu%2F0huqDffLvAj4LylQ6h%2B9JZq4Z3wNKwZu2lpuZ") return res.status(401).json({ status: 401, message: 'Misuse of Key. Blocked.' });
  res.json(manifest);
});

router.get('/stream/:type/:id.json', async (req, res) => {
  const { type, id } = req.params;

  return res.json({
    streams: [
      {
        externalUrl: "https://beeftaco.lol",
        name: "BeefTaco - KEY REQUIRED",
        title: "BeefTaco Addon requires a key. Login to your dashboard to get yours."
      }
    ]
  });


});

router.get('/:userToken/stream/:type/:id.json', async (req, res) => {
  const { userToken, id, type } = req.params;

  const SECRET_KEY = process.env.API_ENCRYPTION_KEY;

  // Kill the old versions.
  if (userToken.includes("apiKey") || userToken.includes("premiumizeApiKey") || userToken.includes("pm-") || userToken.includes("rd-")) {
    logger.info("USER USING OLD API KEY. TOLD TO UPDATE.")
    return res.json({
      streams: [
        {
          externalUrl: "https://beeftaco.lol",
          name: "BeefTaco - UPDATE REQUIRED",
          title: "BeefTaco Addon needs to be updated. Please reinstall."
        }
      ]
    });
  }

  // I believe this is misuse of a key:
  if (userToken === "V1dO4N9p2%2BgUQ5a4cpQ35E5iRu%2F0huqDffLvAj4LylQ6h%2B9JZq4Z3wNKwZu2lpuZ" ||
      userToken === "V1dO4N9p2%252BgUQ5a4cpQ35E5iRu%252F0huqDffLvAj4LylQ6h%252B9JZq4Z3wNKwZu2lpuZ") {
    return res.status(401).json({ status: 401, message: 'Misuse of key. Blocked.' }); // torrentio?
  }


  // Decode the URL-encoded encrypted API key
  const combinedData = Buffer.from(decodeURIComponent(userToken), 'base64');

  // Extract the IV and the encrypted data from the combined data
  const iv = combinedData.slice(0, 16); // IV is the first 16 bytes
  const encryptedDataBuffer = combinedData.slice(16);

  // Create a Decipher object with the AES algorithm and secret key
  const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(SECRET_KEY, 'hex'), iv);

  // Decrypt the data
  let decryptedData = decipher.update(encryptedDataBuffer, 'base64', 'utf8');
  decryptedData += decipher.final('utf8');

  // Assuming the data contains the apiKey and userId separated by '||'
  const parts = decryptedData.split('||');
  const apiKey = parts[0];
  //let userId = parts.length > 1 ? parts[1] : null;
  console.log('apiKey: ', apiKey);

  // SAVE THE REQUEST TO USERS MYSQL
  // FOR STATISTICS.
  // /users/stats/update

  try {
    const results = await handleStreams(id, type, apiKey);
    logger.debug('API KEY seriesId: ' + id);
    res.json(results);
  } catch (error) {
    logger.error(`Error handling stream request for movie ${id}: ${error}`);
    res.status(500).send('Internal server error');
  }
});


export default router;