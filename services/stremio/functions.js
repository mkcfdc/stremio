import { fetchTorrentResults } from '../jackett/functions.js';
import * as premiumize from "../premiumize/functions.js";
import * as rd from "../rd/functions.js";
import * as adb from "../adb/functions.js";
import * as dl from "../debridlink/functions.js";

import logger from '../../utils/logger.js';
import { formatStreamTitle } from '../../utils/stremioFormatting.js';

const STREMIO_URL = process.env.STREMIO_URL || 'https://stremio.beeftaco.lol';

// Middleware
// Validate IMDb ID format (e.g., tt123456789)
export const validateIMDbID = (req, res, next) => {
    const imdbIDRegex = /^tt\d{7,9}$/;
    if (imdbIDRegex.test(req.params.imdbId)) {
        next();
    } else {
        res.status(400).send('Invalid IMDb ID format');
    }
};

// General function to handle stream enhancement for a service
async function enhanceStreamsForService(streams, apiKey, servicePrefix, serviceFunction) {
    if (!apiKey.startsWith(servicePrefix)) return streams;

    const uniqueHashes = [...new Set(streams.map(s => s.infoHash))].slice(0, 5);
    const cleanedApiKey = apiKey.replace(servicePrefix, '');

    try {
        const serviceRes = await serviceFunction(uniqueHashes, cleanedApiKey);
        return streams.map(stream => {
            // const serviceMatch = serviceRes.find(({ magnetLink }) => magnetLink.includes(stream.infoHash));
            const serviceMatch = serviceRes.find(({ magnetLink }) =>
                magnetLink.toLowerCase().includes(stream.infoHash.toLowerCase())
            );

            if (serviceMatch) {
                return {
                    ...stream,
                    name: "🚀BeefTaco: ", // Using the title for naming
                    url: serviceMatch.directLink,
                    isPremium: true
                };
            }
            return stream;
        });
    } catch (error) {
        logger.error(`Error fetching ${servicePrefix.replace('-', '')} streams:`, error);
        return streams; // Return the original streams if the enhancement fails
    }
}

function extractTrackersFromMagnetURI(magnetURI, infoHash, iptKey = null) {
    // Split the URI by '&' to separate parameters
    const parts = magnetURI.split('&');

    // Extract trackers, replace specific hash with iptKey if needed, decode, and prepend 'tracker:'
    let trackers = parts.filter(part => part.startsWith('tr='))
        .map(tracker => {
            // Decode tracker URL
            let url = decodeURIComponent(tracker.substring(3));
            // Replace specific hash with iptKey if present and iptKey is provided
            if (iptKey && url.includes('07f970582698ad0daa1173b277301d6f')) {
                url = url.replace('07f970582698ad0daa1173b277301d6f', iptKey);
            }
            return 'tracker:' + url;
        });

    // Initially add DHT source
    let dhtSource = `dht:${infoHash}`;
    trackers.push(dhtSource);

    // URLs to check for excluding DHT
    const excludeDHTForTrackers = [
        "https://routing.bgp.technology/",
        "https://ssl.empirehost.me/",
        "https://localhost.stackoverflow.tech/"
    ];

    // Check if any tracker matches the specified patterns and remove DHT if so
    const shouldExcludeDHT = trackers.some(tracker =>
        excludeDHTForTrackers.some(url => tracker.includes(url))
    );

    if (shouldExcludeDHT) {
        trackers = trackers.filter(tracker => tracker !== dhtSource);
    }

    return trackers;
}

export async function handleStreams(imdbId, type, apiKey) {

    logger.debug('Sending a fetch to jackett: ' + imdbId + ' and ' + type);

    if (!apiKey.startsWith('pm') &&
        !apiKey.startsWith('user') &&
        !apiKey.startsWith('rd') &&
        !apiKey.startsWith('adb') &&
        !apiKey.startsWith('dl')) {
        return {
            streams: [{
                externalUrl: "https://beeftaco.lol/",
                name: "BeefTaco - REGISTRATION REQUIRED",
                title: "BeefTaco Key Required. Register at beeftaco.lol"
            }]
        };
    }

    const jackettRes = await fetchTorrentResults(imdbId, type);
    if (!jackettRes) return { streams: [] };

    // Map results to include an ipT flag indicating IPTorrents tracker presence
    let streams = jackettRes.Results.map(({ MagnetUri, InfoHash, Title, Seeders, Size, Tracker }) => ({
        infoHash: InfoHash.toLowerCase(),
        sources: extractTrackersFromMagnetURI(MagnetUri, InfoHash.toLowerCase()),
        name: "🧲BeefTaco: ",
        title: formatStreamTitle({ Title, Seeders, Size, Tracker, quality: Title.match(/(1080p|720p|2160p)/)?.[0] || "default" }),
        isPremium: false,
        quality: Title.match(/(1080p|720p|2160p)/)?.[0] || "default",
    }));

    // Now strip streams down to 10.
    streams = streams.slice(0, 10);

    // userId is split out at the route level.
    //logger.debug('API KEY SENT FOR ENRICHMENT IS: ' + apiKey);
    // Enhance streams for Premiumize if applicable
    if (apiKey && apiKey.startsWith('pm-')) streams = await enhanceStreamsForService(streams, apiKey, 'pm-', premiumize.getTranscodedProxy);
    // Enhance streams for Real Debrid if applicable
    if (apiKey && apiKey.startsWith('rd-')) streams = await enhanceStreamsForService(streams, apiKey, 'rd-', rd.checkAvailability);
    // Enhace streams for All Debrid if applicable
    if (apiKey && apiKey.startsWith('adb-')) streams = await enhanceStreamsForService(streams, apiKey, 'adb-', adb.checkInstantAvailability);
    // Enhance streams for Debrid-Link if applicable
    if (apiKey && apiKey.startsWith('dl-')) streams = await enhanceStreamsForService(streams, apiKey, 'dl-', dl.checkAvailability);
    // Enhance nothing when we have a apiKey of 'user-':
    if (apiKey && apiKey.startsWith('user-')) { }

    // Sort streams so that premium ones come first
    streams.sort((a, b) => (a.isPremium === b.isPremium) ? 0 : a.isPremium ? -1 : 1);

    // Delete cache option, display at end of streams.
    if (jackettRes.ttl <= (604800 - 86400)) { // 518400 seconds remaining means 24 hours have passed
        const clearCacheOption = {
            name: 'BeefTaco: Clear cache',
            title: 'Click here to refresh cache. This will open a new window.',
            externalUrl: `${STREMIO_URL}/clearCache/${imdbId}`
        };
        streams.push(clearCacheOption);
    }

    return { streams };
}