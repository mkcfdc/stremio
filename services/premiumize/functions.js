import axios from "axios";
//import crypto from 'crypto';
import qs from 'qs';

//import { setEncrypted, getDecrypted } from "../../middleware/encryption.js";

import logger from "../../utils/logger.js";

const STREMIO_URL = process.env.STREMIO_URL || 'https://stremio.beeftaco.lol';
const API_BASE_URL = process.env.PREMIUMIZE_API || 'https://premiumize.me/api';

export async function getTranscodedProxy(hashes, apiKey) {
  logger.debug(hashes);
  const magnetLinks = hashes.map((hash) => `magnet:?xt=urn:btih:${hash}`);

  const cacheInfo = await checkCache(magnetLinks, apiKey);
  logger.debug(cacheInfo);
  if (!cacheInfo) {
    logger.debug("Failed to retrieve cache information for the provided hashes.");
  }

  const directLinks = cacheInfo
    .filter((item) => item.isTranscoded)
    .filter((item) => item.filesize < 10 * 1024 * 1024 * 1024)
    .map((item) => {
      const hash = item.magnetLink.split(":").pop();
      // Concatenate hash and apiKey with a separator
      const combinedString = `${hash}---${apiKey}`;
      const encodedHash = Buffer.from(combinedString).toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=+$/, '');

      const directLink = `${STREMIO_URL}/streamLink/pm/${encodedHash}.mp4`;

      return { magnetLink: item.magnetLink, directLink };
    });

  return directLinks.filter(Boolean);
}

export async function directLink(magnetLink, apiKey) {
  try {
    //const hash = magnetLink.match(/btih:([a-zA-Z0-9]+)/)[1];

    logger.debug('directLink function called: ' + magnetLink);
    logger.debug('API Key: ' + apiKey);

    //const cacheKey = `streamLink:${hash}`;

    // Attempt to retrieve and decrypt the stream link from Redis
    //let streamLink = await getDecrypted(cacheKey);
    //if (streamLink) {
    //  streamLink = JSON.parse(streamLink);
    //  logger.debug('StreamLink recieved from cache; '+ streamLink);
    //  return streamLink.directLink;
    //}

    // Determine the API URL
    const apiUrl = `${API_BASE_URL}/transfer/directdl`;

    // Prepare the request data
    const requestData = qs.stringify({ src: magnetLink });

    // Make a request to the Premiumize API
    const responseData = await axios.post(apiUrl, requestData, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'Authorization': `Bearer ${apiKey}`
      }
    });

    logger.debug('Premiumize API (directLink function) Response:' + JSON.stringify(responseData.data, null, 2));

    if (
      responseData.data.status === "success" &&
      responseData.data.content &&
      responseData.data.content.length > 0
    ) {
      const streamLink = responseData.data.content.sort((a, b) => b.size - a.size)[0].stream_link;

      // Encrypt and store the stream link in Redis with a 24-hour expiration
      //await setEncrypted(cacheKey, JSON.stringify({directLink: streamLink }), 86400);
      logger.debug('streamLink stored in cache: ' + streamLink);

      return streamLink;
    }

    //throw new Error("Unsuccessful API response: " + JSON.stringify(responseData.data));
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}


async function checkCache(magnetLinks, apiKey) {
  //const cacheKey = crypto.createHash('md5').update(magnetLinks[0]).digest('hex');
  //const redisCacheKey = `checkCache:pm:${cacheKey}`;
  //const cachedData = await getDecrypted(redisCacheKey);
  //if (cachedData) return JSON.parse(cachedData);

  try {
    const { data } = await axios.get(`${API_BASE_URL}/cache/check`, {
      params: { items: magnetLinks }, // Assuming the API expects 'items' as a query parameter
      headers: {
        'Authorization': `Bearer ${apiKey}` // Correctly formatted Authorization header
      }
    });
    if (!data) return null;

    const result = magnetLinks.map((link, index) => ({
      magnetLink: link,
      isCached: data.response[index], // Assuming your API response has 'response', 'transcoded', etc. arrays
      isTranscoded: data.transcoded[index],
      filename: data.filename[index],
      filesize: data.filesize[index],
    }));

    //await setEncrypted(redisCacheKey, JSON.stringify(result), 86400);
    return result;

  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
  }
}
