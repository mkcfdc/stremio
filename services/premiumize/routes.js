import express from "express";

import { getDecrypted, setEncrypted } from '../../middleware/encryption.js';

import logger from "../../utils/logger.js";
import * as premiumize from "./functions.js";

const router = express.Router();

router.get('/:hash.mp4', async (req, res) => {
  const encodedHash = req.params.hash;

  // Decode the URL-safe Base64 hash and split to get the magnet hash and apiKey
  const decodedString = Buffer.from(encodedHash
    .replace(/-/g, '+')
    .replace(/_/g, '/'),
    'base64')
    .toString('utf-8');

  const [hash, apiKey] = decodedString.split('---');
  const magnetLink = 'magnet:?xt=urn:btih:' + hash;

  if (!magnetLink || !apiKey)
    return res.status(400).send('Missing magnetLink or apiKey');

  try {
    logger.debug('Entering DirectLink function.');

    // Attempt to retrieve the URL from cache
    const cachedUrl = await getDecrypted('pm:' + encodedHash);
    const json = JSON.parse(cachedUrl);
    if (cachedUrl) return res.redirect(json.directLink);

    const directLinkUrl = await premiumize.directLink(magnetLink, apiKey);
    if (!directLinkUrl) {
      logger.debug('No direct link found');
      return res.status(404).send('No direct link found');
    }
    logger.debug('recieved apikey: ' + apiKey);
    logger.debug('sending: ' + directLinkUrl);

    // Cache the URL for 300 seconds to prevent multiple calls to server.
    await setEncrypted('pm:' + encodedHash, JSON.stringify({ directLink: directLinkUrl }), 300);

    return res.redirect(directLinkUrl);
  } catch (error) {
    logger.error(`Error occurred: ${error.message}`, {
      stack: error.stack,
      // Additional context or metadata
    });
    res.status(500).send('Internal Server Error');
  }
});

export default router;